/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include <iostream>
#include <vector>
#include <algorithm>

namespace SPF {

    class Path {
        std::vector<int> path;
        int cost = 0;

    public:
        using iterator = std::vector<int>::iterator;
        using const_iterator = std::vector<int>::const_iterator;

        void addStep(int id);

        void setCost(int cost);

        int getCost() const;

        void reverse();

        bool isEmpty() const;

        iterator begin();

        iterator end();

        const_iterator cbegin() const;

        const_iterator cend() const;

        friend std::ostream &operator<<(std::ostream &o, const Path &a);

        bool operator==(const Path &other) const;

        bool operator!=(const Path &other) const;
    };

}


