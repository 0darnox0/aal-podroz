/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include <chrono>
#include <iostream>

using TimeVar = std::chrono::high_resolution_clock::time_point;
#define timeNow() std::chrono::high_resolution_clock::now()
#define duration(a) std::chrono::duration_cast<std::chrono::milliseconds>(a).count()

class StopWatch {
    TimeVar lastTime;
    std::string message;

public:

    StopWatch(const std::string &message = "") : message(message) {
        lastTime = timeNow();
    }

    void watch() {
        TimeVar now = timeNow();
        std::cout << message << duration(now - lastTime) << " ms \n";
        lastTime = now;
    }

    void reset() {
        lastTime = timeNow();
    }

};


