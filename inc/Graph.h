/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include <map>
#include <list>
#include <algorithm>
#include <ostream>
#include <iostream>
#include "Vertex.h"
#include "Edge.h"


template<typename T>
class Graph {
public:
    static int counter;
    /* Made public on purpose to avoid redundant calls to id2Vertex and connections structures */
    using edge_t = Edge<T>;
    using vertex_t = Vertex<T>;

    bool directed;
    int id = counter++;
    std::map<T, vertex_t *> id2Vertex;
    std::vector<edge_t *> edges;
    std::map<T, std::list<edge_t *> > connections;

    explicit Graph(bool directed);

    Graph(const Graph &other);

    virtual ~Graph();

    void addEdge(const T &first, const T &second, int cost, int time);

    void addVertex(const T &id, int cost);

    vertex_t *getVertex(const T &id);

    edge_t *getEdge(const T &from, const T &to);

    const std::list<std::pair<T, T> > getEdges() const;

    const std::list<T> getAdjacent(const T &id) const;

    unsigned long long int getVerticesCount() const;

    unsigned long long int getEdgesCount() const;

    unsigned long long int getGroupsCount() const;

    std::vector<T> getVerticesInGroup(int group) const;

    friend std::ostream &operator<<(std::ostream &os, const Graph<T> &g) {
        os << g.getVerticesCount() << " " << g.getEdgesCount() << " " << g.getGroupsCount() << "\n";

        for(auto v: g.id2Vertex) {
            os << v.second->getCost() << " ";
        }
        os << "\n";

        for(auto e: g.edges) {
            os << e->getFirst()->getID() << " " << e->getSecond()->getID() << " "
            << e->getCost() << " " << e->getTime() << "\n";
        }

        for(unsigned int i = 0; i < g.getGroupsCount(); i++) {
            auto inGroup = g.getVerticesInGroup(i);
            os << inGroup.size();
            for (auto v: inGroup) {
                os << " " << v;
            }
            os << "\n";
        }

        return os;
    }

};

template<typename T>
int Graph<T>::Graph::counter = 0;

template<typename T>
Graph<T>::Graph(bool directed): directed(directed) {

}

template<typename T>
Graph<T>::~Graph() {
    for (unsigned int i = 0; i < edges.size(); i++) {
        delete edges[i];
    }
    for (unsigned int i = 0; i < id2Vertex.size(); i++) {
        if (id2Vertex[i]->getGraph() == this)  {
            delete id2Vertex[i];
        }

    }
    id2Vertex.clear();
    connections.clear();
    edges.clear();
}

template<typename T>
void Graph<T>::addEdge(const T &first, const T &second, const int cost, const int time) {
    if (first == second) return;

    if (id2Vertex.find(first) == id2Vertex.end())
        throw std::runtime_error("No vertex in graph with id: " + first);
    if (id2Vertex.find(second) == id2Vertex.end())
        throw std::runtime_error("No vertex in graph with id: " + second);

    for (auto &edge: connections[first]) {
        if (edge->hasVertex(second)) return;
    }

    auto edge = new edge_t(id2Vertex[first], id2Vertex[second], directed, cost, time);

    connections[first].push_back(edge);
    if (!directed) {
        connections[second].push_back(edge);
    }

    edges.push_back(edge);
}

template<typename T>
void Graph<T>::addVertex(const T &id, const int cost) {
    if (id2Vertex.find(id) != id2Vertex.end()) return;

    auto *v = new vertex_t(id, this, cost);
    id2Vertex[id] = v;
}


template<typename T>
const std::list<std::pair<T, T> > Graph<T>::getEdges() const {
    std::list<std::pair<T, T> > list;

    for (auto &edge: edges) {
        list.push_back(std::make_pair(edge->getFirst()->getID(), edge->getSecond()->getID()));
    }

    return list;
}

template<typename T>
const std::list<T> Graph<T>::getAdjacent(const T &id) const {
    std::list<T> adjacent;

    for (auto &edge: connections.at(id)) {
        adjacent.push_back(edge->getAdjacent(id)->getID());
    }

    return adjacent;
}


template<typename T>
Vertex<T> *Graph<T>::getVertex(const T &id) {
    return id2Vertex.at(id);
}

template<typename T>
Edge<T> *Graph<T>::getEdge(const T &from, const T &to) {
    for (auto &edge: connections[from]) {
        if (edge->hasVertex(to)) return edge;
    }

    return nullptr;
}

template<typename T>
Graph<T>::Graph(const Graph &other) {

    directed = other.directed;

    for (auto const &vertex: other.id2Vertex) {
        auto *new_vertex = new vertex_t(*vertex.second);
        new_vertex->setGraph(this);
        id2Vertex[vertex.first] = new_vertex;
    }

    for(auto const &edge: other.edges){
        addEdge(edge->getFirst()->getID(), edge->getSecond()->getID(), edge->getCost(), edge->getTime());
    }
}

template<typename T>
unsigned long long int Graph<T>::getEdgesCount() const {
    return edges.size();
}

template<typename T>
unsigned long long int Graph<T>::getGroupsCount() const {
    std::set<int> groups;
    for (auto v: id2Vertex) {
        groups.insert(v.second->getGroup());
    }

    groups.erase(-1);

    return groups.size();
}

template<typename T>
unsigned long long int Graph<T>::getVerticesCount() const {
    return id2Vertex.size();
}

template<typename T>
std::vector<T> Graph<T>::getVerticesInGroup(int group) const {
    std::vector<T> vertices;
    for (auto v: id2Vertex) {
        if (v.second->getGroup() == group) {
            vertices.push_back(v.first);
        }
    }

    return vertices;
}
