#include <random>
#include <ctime>

/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include "Definitions.h"

namespace SPF {

    class GraphGenerator {
        using UID = std::uniform_int_distribution<unsigned int>;

        std::mt19937 e1;

    public:
        GraphGenerator() {
            e1.seed(static_cast<unsigned int>(time(nullptr)));
        }

        Graph *generateGraph(unsigned int N,
                             double D,
                             unsigned int G,
                             unsigned int max_vertex_cost = 100,
                             unsigned int max_edge_cost = 100,
                             unsigned int max_edge_time = 100);

        Graph *generateHardGraph(unsigned int N,
                                 unsigned int G,
                                 unsigned int max_vertex_cost = 100,
                                 unsigned int max_edge_cost = 100,
                                 unsigned int max_edge_time = 100);

    private:
        void createVertices(Graph *graph, unsigned int N, unsigned int max_cost);

        void createGroups(Graph *graph, unsigned int N, unsigned int G);

        void createEdges(Graph *graph, unsigned int N, double D, unsigned int max_cost, unsigned int max_time);

    };

}



