/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include <vector>
#include <set>
#include <limits>
#include "Edge.h"
#include "Graph.h"

template<typename T>
class Graph;

enum STATUS {
    VISITED, NOT_VISITED
};

template<typename T>
class Vertex {
private:
    T id;
    int cost;
    int group;
    int original_group;
    STATUS state;
    bool final;
    Graph<T> *graph;
    int lowestCost = std::numeric_limits<int>::max();
    Vertex *cameFrom = nullptr;

public:
    Vertex() = delete;

    explicit Vertex(T id, Graph<T> *graph, int cost = 0) : id(id), cost(cost), group(-1), state(NOT_VISITED),
                                                           final(false), graph(graph) {
        original_group = group;
    }

    Vertex(const Vertex &other) {

        id = other.id;
        cost = other.cost;
        group = other.group;
        state = other.state;
        final = other.final;
    }

    ~Vertex() {};

    std::list<Edge<T> *> getConnections() {
        return graph->connections[id];
    }

    void setGraph(Graph<T> *graph) {
        this->graph = graph;
    }

    Graph<T> *getGraph() {
        return graph;
    }

    void setGroup(int group) {
        this->group = group;
        original_group = group;
    }

    void unsetGroup() {
        this->group = -1;
    }

    int getOriginalGroup() const {
        return original_group;
    }

    void setCost(int cost) {
        this->cost = cost;
    }

    bool isVisited() const {
        return state == VISITED;
    }

    void markVisited() {
        state = VISITED;
    }

    void markNotVisited() {
        state = NOT_VISITED;
    }

    void setFinal(bool final) {
        this->final = final;
    }

    bool isFinal() const {
        return final;
    }

    int getGroup() const {
        return group;
    }

    int getCost() const {
        return cost;
    }

    int getLowestCost() const {
        return lowestCost;
    }

    void setLowestCost(int lowestCost) {
        Vertex::lowestCost = lowestCost;
    }

    Vertex *getCameFrom() const {
        return cameFrom;
    }

    void setCameFrom(Vertex *cameFrom) {
        Vertex::cameFrom = cameFrom;
    }

    inline T getID() const {
        return id;
    }

    bool operator==(const Vertex &other) const {
        return (id == other.id) && (graph == other.graph);
    }

    bool operator!=(const Vertex &other) const {
        return !(*this == other);
    }

    bool operator<(const Vertex &other) const {
        return lowestCost < other.lowestCost;
    }

    bool operator>(const Vertex &other) const {
        return lowestCost > other.lowestCost;
    }
};

