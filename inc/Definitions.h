/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include "Graph.h"

namespace SPF {
    using Graph = Graph<int>;
    using Edge = Edge<int>;
    using Vertex = Vertex<int>;

    class CompareVertices {
    public:
        bool operator()(Vertex *l, Vertex *r) {
            if (l->getLowestCost() > r->getLowestCost()) return true;
            else if (l->getLowestCost() == r->getLowestCost()) return l->getID() > r->getID();
            return false;
        }
    };
}