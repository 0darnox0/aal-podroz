/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include <utility>


template<typename T>
class Vertex;

template<typename T>
class Edge {

protected:
    Vertex<T> *first;
    Vertex<T> *second;
    bool directed;

    int cost;
    int time;

public:
    Edge() = delete;

    Edge(Vertex<T> *s, Vertex<T> *e, bool directed, int cost = 0, int time = 0)
            : first(s), second(e), directed(directed), cost(cost), time(time) {}

    ~Edge() {}

    int getCost() const {
        return cost;
    }

    int getTime() const {
        return time;
    }

    inline bool isDirected() const {
        return directed;
    }

    inline bool hasVertex(const T &id) const {
        return (first->getID() == id || second->getID() == id);
    }

    inline const std::pair<Vertex<T> *, Vertex<T> *> getVertices() const {
        return std::make_pair(first, second);
    }

    bool operator==(const Edge &other) const {
        return (first == other.first && second == other.second) ||
               (!directed && (second == other.first && first == other.second));
    }

    bool operator!=(const Edge &other) const {
        return !(*this == other);
    }

    Vertex<T> *getAdjacent(T id) {
        if (first->getID() == id) return second;
        else if (second->getID() == id) return first;
        else return nullptr;
    }

    Vertex<T> *getAdjacent(Vertex<T> *v) {
        if (first == v) return second;
        else if (second == v) return first;
        else return nullptr;
    }

    Vertex<T> *getFirst() {
        return first;
    }

    Vertex<T> *getSecond() {
        return second;
    }
};


