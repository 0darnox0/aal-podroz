/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include "Graph.h"
#include <iostream>
#include <queue>
#include <limits>
#include <set>
#include "Definitions.h"
#include "Path.h"

namespace SPF {

    class Algorithm {
    private:
        Graph *graph;
        std::priority_queue<Vertex *, std::vector<Vertex *>, CompareVertices> cheapest;
        int timeCost;

    public:
        explicit Algorithm(Graph *graph, int timeCost) : graph(graph), timeCost(timeCost) {}

        Path findPath(int src, int dest);

        int pathCost(const Path &path);

    private:
        int moveCost(Edge *e, Vertex *current);

        int edgeCost(Edge *edge);

        void reset();

    };

}
