/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#pragma once

#include "Definitions.h"
#include <iostream>

/* Input format:
 * 5 8 3           // nVertices, nEdges, nGroups
 * 8 5 2 4 7       // cV0, cV1, cV2, ...
 * 0 1 5 5         // eFrom0, eTo0, eCost0, eTime0
 * 1 2 8 5         // eFrom1, eTo1, eCost1, eTime1
 * 2 3 9 6         // ...
 * 3 4 1 6         // ...
 * 4 5 9 3         // ...
 * 0 3 2 1         // ...
 * 2 5 8 1         // ...
 * 2 4 1 1         // ...
 * 5 0 1 2 3 5     // capacityGroup0, vID, vID, ...
 * 2 1 5           // capacityGroup1, vID, vID, ...
 * 1 4             // capacityGroup2, vID, vID, ...
 * 0 5 2           // findFrom, findTo, timeCost
 */

namespace SPF {

    class GraphReader {
    public :
        static SPF::Graph * readGraph(std::istream &input);

        static void saveGraph(Graph g);
    };

}