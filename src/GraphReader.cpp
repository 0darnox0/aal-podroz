/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#include "../inc/GraphReader.h"
#include <iostream>
#include <fstream>

SPF::Graph * SPF::GraphReader::readGraph(std::istream &input = std::cin) {
    auto graph = new Graph(false);

    int nVertices, nEdges, nGroups;

    input >> nVertices >> nEdges >> nGroups;

    int vCost;
    for (int v = 0; v < nVertices; v++) {
        input >> vCost;
        graph->addVertex(v, vCost);
    }

    int eFrom, eTo, eCost, eTime;
    for (int e = 0; e < nEdges; e++) {
        input >> eFrom >> eTo >> eCost >> eTime;
        graph->addEdge(eFrom, eTo, eCost, eTime);
    }

    int gCapacity, vID;
    for (int g = 0; g < nGroups; g++) {
        input >> gCapacity;
        while (gCapacity--) {
            input >> vID;
            graph->getVertex(vID)->setGroup(g);
        }
    }

    return graph;
}

void SPF::GraphReader::saveGraph(SPF::Graph g) {
    std::cout << g;
}
