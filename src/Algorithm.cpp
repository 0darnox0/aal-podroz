/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#include "../inc/Algorithm.h"
#include "../inc/StopWatch.h"

namespace SPF {
    int Algorithm::pathCost(const Path &path) {
        int sum = 0;
        std::set<int> visitedGroups;

        auto pathIterator = path.cbegin();
        Vertex *currentVertex = graph->getVertex(*pathIterator++);

        for (; pathIterator != path.cend(); pathIterator++) {
            sum += edgeCost(graph->getEdge(currentVertex->getID(), *pathIterator));

            currentVertex = graph->getVertex(*pathIterator);
            if (currentVertex->getOriginalGroup() == -1 ||
                visitedGroups.find(currentVertex->getOriginalGroup()) == visitedGroups.end()) {

                sum += currentVertex->getCost();
                visitedGroups.insert(currentVertex->getOriginalGroup());
            }
        }

        return sum;
    }

    Path Algorithm::findPath(int src, int dest) {
        reset();

        Path path;

        std::vector<Graph *> createdWorlds;

        std::vector<bool> mirroredVertex(graph->getVerticesCount(), false);

        graph->getVertex(dest)->setFinal(true);

        Vertex *currentVertex;
        graph->getVertex(src)->setLowestCost(0);
        cheapest.emplace(graph->getVertex(src));
        while (!cheapest.empty()) {
            currentVertex = cheapest.top();
            cheapest.pop();

            if (currentVertex->isVisited()) continue;

        //    std::cout << currentVertex->getID() << "\n";

            if (currentVertex->isFinal()) {
                while (currentVertex->getCameFrom() != nullptr) {
                    path.addStep(currentVertex->getID());
                    currentVertex = currentVertex->getCameFrom();
                }
                path.addStep(src);
                path.reverse();

                path.setCost(pathCost(path));

                for(auto g: createdWorlds) {
                    delete g;
                }

                return path;
            }

            if (currentVertex->getGroup() != -1 && !mirroredVertex[currentVertex->getID()]) {
                mirroredVertex[currentVertex->getID()] = true;
                auto *new_graph = new Graph(*currentVertex->getGraph());
                createdWorlds.push_back(new_graph);

                for (auto &v: new_graph->id2Vertex) {
                    if (v.second->getGroup() == currentVertex->getGroup()) {
                        v.second->unsetGroup();
                        v.second->setCost(0);
                    }
                    v.second->markNotVisited();
                    v.second->setLowestCost(std::numeric_limits<int>::max());
                    v.second->setCameFrom(nullptr);
                }
                currentVertex->unsetGroup();

                auto *currentVertexCopy = new_graph->getVertex(currentVertex->getID());

                auto *e = new Edge(currentVertex, currentVertexCopy, false, 0, 0);

                currentVertex->getGraph()->connections[currentVertex->getID()].push_back(e);
//                currentVertex->getGraph()->edges.push_back(e);
                new_graph->connections[currentVertex->getID()].push_back(e);
                new_graph->edges.push_back(e);
            }

            for (auto &edge: currentVertex->getConnections()) {
                auto *adj = edge->getAdjacent(currentVertex);
                if (!adj->isVisited()) {
                    if (currentVertex->getLowestCost() + moveCost(edge, adj) < adj->getLowestCost()) {
                        adj->setLowestCost(currentVertex->getLowestCost() + moveCost(edge, adj));
                        adj->setCameFrom(currentVertex);
                    }
                    cheapest.push(adj);
                }
            }
            currentVertex->markVisited();
        }

        for(auto g: createdWorlds) {
            delete g;
        }

        return path;
    }

    int Algorithm::moveCost(Edge *e, Vertex *adj) {
        return e->getCost() + e->getTime() * timeCost + adj->getCost();
    }

    int Algorithm::edgeCost(Edge *edge) {
        return edge->getCost() + edge->getTime() * timeCost;
    }

    void Algorithm::reset() {
        while (!cheapest.empty()) cheapest.pop();
    }
}