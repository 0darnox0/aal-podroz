/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#include "../inc/Path.h"

namespace SPF {

    void Path::addStep(const int id) {
        if (!path.empty() && path.back() == id) return;

        path.push_back(id);
    }

    void Path::setCost(int cost) {
        this->cost = cost;
    }

    int Path::getCost() const{
        return cost;
    }

    void Path::reverse() {
        std::reverse(path.begin(), path.end());
    }

    bool Path::isEmpty() const {
        return path.empty();
    }

    Path::iterator Path::begin() {
        return path.begin();
    }

    Path::iterator Path::end() {
        return path.end();
    }

    Path::const_iterator Path::cbegin() const {
        return path.cbegin();
    }

    Path::const_iterator Path::cend() const {
        return path.cend();
    }

    std::ostream &operator<<(std::ostream &o, const Path &a) {
        for (auto it = a.cbegin(); it != a.cend(); it++) {
            o << *it << " ";
        }

        return o;
    }

    bool Path::operator==(const Path &other) const {
        return cost == other.cost && path == other.path;
    }

    bool Path::operator!=(const Path &other) const {
        return !(*this == other);
    }

}
