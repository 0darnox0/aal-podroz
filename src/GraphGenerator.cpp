/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#include "../inc/GraphGenerator.h"
#include "../inc/StopWatch.h"
#include <iterator>
#include <algorithm>
#include <iostream>

namespace SPF {

    Graph * GraphGenerator::generateGraph(unsigned int N,
                                          double D,
                                          unsigned int G,
                                          unsigned int max_vertex_cost,
                                          unsigned int max_edge_cost,
                                          unsigned int max_edge_time) {

        if (D > 1) throw std::runtime_error("Density must be not greater than 1");

        auto graph = new Graph(false);


//        std::cout << "Generating graph... " << std::endl;
//        StopWatch grap("Done in ");
        createVertices(graph, N, max_vertex_cost);
        createGroups(graph, N, G);
        createEdges(graph, N, D, max_edge_cost, max_edge_time);
//        grap.watch();

        return graph;
    }

    Graph * GraphGenerator::generateHardGraph(unsigned int N,
                                              unsigned int G,
                                              unsigned int max_vertex_cost,
                                              unsigned int max_edge_cost,
                                              unsigned int max_edge_time) {

        auto graph = new Graph(false);

        UID cost_generator(0, max_edge_cost);
        UID time_generator(0, max_edge_time);

        G += 0;
        max_vertex_cost += 0;

        createVertices(graph, N, 1);

        for (unsigned int i = 1; i < N - 1; i++) {
            graph->getVertex(i)->setGroup(i);
            graph->addEdge(0, i, 1, 1);
        }

        graph->getVertex(N - 1)->setGroup(N - 1);
        graph->getVertex(N - 1)->setCost(N * N);
        graph->addEdge(0, N - 1, N * N, N * N);

        return graph;
    }

    void GraphGenerator::createVertices(Graph *graph, unsigned int N, unsigned int max_cost) {
        UID vertex_cost_generator(0, max_cost);

        // Add id2Vertex
        for (unsigned int i = 0; i < N; i++) {
            graph->addVertex(i, vertex_cost_generator(e1));
        }
    }

    void GraphGenerator::createGroups(Graph *graph, unsigned int N, unsigned int G) {
        if (G == 0) return;

        std::vector<int> selected_vertices(N - 1);
        std::iota(selected_vertices.begin(), selected_vertices.end(), 0);

        std::shuffle(selected_vertices.begin(), selected_vertices.end(), e1);

        // Select id2Vertex that belongs to any group
        unsigned int vertices_in_group = UID(G, N)(e1);
        selected_vertices.resize(vertices_in_group); // id2Vertex with group

        // Assign id2Vertex to groups
        unsigned int stride;
        unsigned int last_stride = 0;
        unsigned int current_group = 0;

        for (; current_group < G - 1; current_group++) {
            stride = UID(last_stride, vertices_in_group - (G - 1) + current_group)(e1);
            for (unsigned int i = last_stride; i < stride; i++) {
                graph->getVertex(selected_vertices[i])->setGroup(current_group);
            }
            last_stride = stride;
        }

        for (unsigned int i = last_stride; i < vertices_in_group; i++) {
            graph->getVertex(selected_vertices[i])->setGroup(current_group);
        }
    }

    void GraphGenerator::createEdges(Graph *graph, unsigned int N, double D, unsigned int max_cost,
                                     unsigned int max_time) {
        UID cost_generator(0, max_cost);
        UID time_generator(0, max_time);
        UID vertex_generator(0, N - 1);
        std::uniform_real_distribution<double> edge_selector(0.0, 1.0);

        // Create spanning tree
        std::set<int> s, t;
        for (unsigned int i = 0; i < N; ++i) {
            s.insert(s.end(), i);
        }

        auto vi = s.begin();
        std::advance(vi, vertex_generator(e1));

        int current_node = *vi;
        s.erase(current_node);
        t.insert(current_node);

        while (!s.empty()) {
            int neighbour = vertex_generator(e1);
            if (t.find(neighbour) == t.end()) {
                graph->addEdge(current_node, neighbour, cost_generator(e1), time_generator(e1));
                s.erase(neighbour);
                t.insert(neighbour);
            }
            current_node = neighbour;
        }

        int w = -1;
        int u = 1;
        int n = N;
        while(u < n) {
            double r = edge_selector(e1);
            w = static_cast<int>(w + 1 + floor(log(1 - r) / log(1 - D)));
            while(w >= u && u < n) {
                w = w - u;
                u = u + 1;
            }
            if (u < n) {
                graph->addEdge(u, w, cost_generator(e1), time_generator(e1));
            }
        }

//        for (unsigned int i = 0; i < N; i++) {
//            for (unsigned int j = i + 1; j < N; j++) {
//                double r = edge_selector(e1);
//                if(r <= D) graph->addEdge(i, j, cost_generator(e1), time_generator(e1));
//            }
//        }
    }
}