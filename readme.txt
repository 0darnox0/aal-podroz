﻿# AAL-14-LS podróż

## Autor
Konrad Bereda

## Zadanie

Dane są miasta połączone siecią dróg. Dla każdej drogi znany jest czas przejazdu w
godzinach. Wjazd do każdego miasta oraz na każdą drogę jest płatny (koszt ustalony jest
oddzielnie dla każdego miasta i drogi). Miasta mogą należeć do grup miast partnerskich -
wjazd do takiego miasta zwalnia z opłat za wjazd do pozostałych miast partnerskich. Każda
godzina podróży ma ustalony koszt (niezależny od drogi). Należy znaleźć najtańszą opcję
podróży pomiędzy dwoma wyróżnionymi miastami.

## Obsługa programu

Uruchomienie programu ze standardowego wejścia:
```bash
  PathFinder [OPTION...]

  -m, --mode arg  Execution modes:
                          1 - use standard I/O
                              2 - generate random instance and solve
                                  3 - benchmark
                                      4 - unit tests (default: 1)
  -h, --help      Print help

 Options for mode m2 and m3: options:
  -c, --city arg     Number of cities (default: 10)
  -d, --density arg  Density od roads between cities (default: 0.5)
  -g, --group arg    Number of groups (default: 4)

 Options for mode m3: options:
  -p, arg  Number of tested problems (default: 10)
  -s, arg  Increases problem size by given number (default: 5)
  -l, arg  Number of instances tested for every problem (default: 10)
```

## Metoda rozwiązania
Roziązanie bazuje na algorytmie Dijkstry. Graf miast przeglądany jest w kolejności od miast
znajdujących się najbliżej miasta startowego. Jeśli algorytm napotka miasto należące do 
grupy, którego jeszcze nie odwiedził to tworzona jest wirtualna kopia obecnego grafu, a wszystkie 
miasta z tej samej grupy, w noym grafie, zmieniają koszt dotarcia do nich na 0.

## Struktura programu
```bash
.
├──inc  - pliki nagłówkowe
│  ├───Algorithm.h  - klasa algorytmu rozwiązującego zadanie
│  ├───cxxopts.h  - zewnętrzna biblioteka do parsowania argumentów
│  ├───Definitions.h  - nagłówek zawierający przydatne makra
│  ├───Edge.h  - klasa reprezentująca krawędź (drogę)
│  ├───Graph.h  - struktura przechowująca graf miast
│  ├───GraphGenerator.h  - klasa do generowania losowych grafów
│  ├───GraphReader.h  - klasa wczytująca graf w określonym formacie
│  ├───Path.h  - klasa reprezentująca ścieżkę w grafie
│  └───Vertex.h  - klasa reprezentująca wierzchołek (miasto)
├──res  - zasoby dodatkowe
│  ├───test  - przykłady testowe
│  │  ├───in  - pliki wejściowe testów
│  │  └───out  - pliki wyjściowe testów
│  └───web  - pliki potrzebne dla GraphCreator.html
├──src  - pliki źródłowe
│  ├───Algorithm.cpp  - definicje dla pliku Algorithm.h
│  ├───GraphGenerator.cpp  - definicje dla pliku GraphGenerator.h
│  ├───GraphReader.cpp  - definicje dla pliku GraphReader.h
│  └───Path.cpp  - definicje dla pliku Path.h
├──readme.txt  - informacje o projekcie
├──GraphCreator.html  - narzędzie do tworzenia i wizualizacji sieci miast
├──main.cpp  - plik startowy, zawiera CLI
└──CMakeLists.txt  - plik programu cmake
```
