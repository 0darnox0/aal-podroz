/* Author: Konrad Bereda
 * Problem: AAL-14-LS podróż
 */

#include "inc/StopWatch.h"
#include <iostream>
#include <fstream>
#include "inc/Algorithm.h"
#include "inc/GraphReader.h"
#include "inc/GraphGenerator.h"
#include "inc/cxxopts.h"
#include <windows.h>
#include <chrono>



using TimeVar = std::chrono::high_resolution_clock::time_point;
#define timeNow() std::chrono::high_resolution_clock::now()
#define duration(a) std::chrono::duration_cast<std::chrono::milliseconds>(a).count()

void unitTests();

void processArguments(const cxxopts::Options &options, const cxxopts::ParseResult &result);

void processMode1();
void processMode2(const cxxopts::ParseResult &result);
void processMode3(const cxxopts::ParseResult &result);
void processMode5(const cxxopts::ParseResult &result);

SPF::Path runAlgorithm(SPF::Graph *graph, int from, int to, int timeCost);

void processMode4();

int main(int argc, char *argv[]) {
    cxxopts::Options options("PathFinder", "Program for solving 'AAL-14-LS podroz' by Konrad Bereda");
    options.show_positional_help();

    options.add_options()
            ("m,mode", "Execution modes:                                                    "
                       "1 - use standard I/O                                            "
                       "2 - generate random instance and solve                          "
                       "3 - benchmark                                                   "
                       "4 - unit tests                                                  "
                       "5 - hard benchmark ", cxxopts::value<unsigned int>()->default_value("1"))
            ("h,help", "Print help");

    options.add_options("Options for mode m2 and m3:")
            ("c,city",    "Number of cities", cxxopts::value<unsigned int>()->default_value("100"))
            ("d,density", "Density od roads between cities",  cxxopts::value<double>()->default_value("0.00001"))
            ("g,group",   "Number of groups", cxxopts::value<unsigned int>()->default_value("4"));

    options.add_options("Options for mode m3:")
            ("p", "Number of tested problems", cxxopts::value<unsigned int>()->default_value("10"))
            ("s", "Increases problem size by given number", cxxopts::value<unsigned int>()->default_value("100"))
            ("l", "Number of instances tested for every problem", cxxopts::value<unsigned int>()->default_value("10"));

    auto result = options.parse(argc, argv);

    processArguments(options, result);
}

void processArguments(const cxxopts::Options &options, const cxxopts::ParseResult &result) {
    if (result.count("help")) {
        std::cout << options.help({"", "Options for mode m2 and m3:", "Options for mode m3:"}) << std::endl;
        exit(0);
    }

    if (result.count("mode")) {
        unsigned int mode = result["mode"].as<unsigned int>();

        if (mode == 1) {
            processMode1();
        } else if (mode == 2) {
            processMode2(result);
        } else if (mode == 3) {
            processMode3(result);
        } else if (mode == 4) {
            processMode4();
        } else if (mode == 5) {
            processMode5(result);
        } else {
            std::cout << "Invalid value of parameter 'mode' specified" << std::endl;
            exit(1);
        }
    } else {
        std::cout << options.help({"", "Options for mode m2 and m3:", "Options for mode m3:"}) << std::endl;
        exit(0);
    }
}

void processMode1() {
    int timeCost, from, to;
    auto graph = SPF::GraphReader::readGraph(std::cin);
    std::cin >> from >> to >> timeCost;

    auto path = runAlgorithm(graph, from, to, timeCost);

    std::cout << path.getCost() << std::endl;
    std::cout << path;
}

void processMode2(const cxxopts::ParseResult &result) {
    SPF::GraphGenerator gg;
    std::mt19937 e1;
    e1.seed(static_cast<unsigned int>(time(nullptr)));

    unsigned int cities = result["c"].as<unsigned int>();
    double       density = result["d"].as<double>();
    unsigned int groups = result["g"].as<unsigned int>();

    if (density < 0 || density > 1) {
        std::cout << "Density must be between 0 and 1" << std::endl;
        exit(1);
    }

    auto graph = gg.generateGraph(cities, density, groups);

    std::uniform_int_distribution<unsigned int> city_selector(0, graph->getVerticesCount() - 1);

    unsigned int from = city_selector(e1);
    unsigned int to = city_selector(e1);
    int timeCost = std::uniform_int_distribution<unsigned int>(0, 100)(e1);

//    std::cout << *graph;
    std::cout << from << " " << to << " " << timeCost << std::endl << std::endl;

    auto path = runAlgorithm(graph, from, to, timeCost);

    std::cout << path.getCost() << std::endl;
    std::cout << path;
}

void processMode3(const cxxopts::ParseResult &result) {
    SPF::GraphGenerator gg;
    std::mt19937 e1;
    e1.seed(static_cast<unsigned int>(time(nullptr)));

    unsigned int cities = result["c"].as<unsigned int>();
    double        density = result["d"].as<double>();
    unsigned int groups = result["g"].as<unsigned int>();

    if (density < 0 || density > 1) {
        std::cout << "Density must be between 0 and 1" << std::endl;
        exit(1);
    }

    unsigned int problems = result["p"].as<unsigned int>();
    unsigned int step = result["s"].as<unsigned int>();
    unsigned int loop = result["l"].as<unsigned int>();

    for (unsigned int p = 0; p < problems; p++) {
        long long averageTime = 0;
        long long edges = 0;
        for (unsigned int l = 0; l < loop; l++) {
            auto graph = gg.generateGraph(cities, density, groups);

            std::uniform_int_distribution<unsigned int> city_selector(0, graph->getVerticesCount() - 1);

            int from = city_selector(e1);
            int to = city_selector(e1);
            int timeCost = std::uniform_int_distribution<unsigned int>(0, 100)(e1);

            TimeVar startTime = timeNow();
            runAlgorithm(graph, from, to, timeCost);
            TimeVar endTime = timeNow();

            averageTime += duration(endTime - startTime);
            edges += graph->getEdgesCount();
            delete graph;
        }
        averageTime = averageTime / loop;
        edges = edges / loop;
        std::cout << "V=" << cities << ", E=" << edges << ": " << averageTime << " ms \n";
        cities += step;
    }
}

void processMode4() {
    unitTests();
}

SPF::Path runAlgorithm(SPF::Graph *graph, int from, int to, int timeCost) {
    SPF::Algorithm alg(graph, timeCost);
    return alg.findPath(from, to);
}

SPF::Path runAlgorithmFromFile(const std::string &path) {
    std::fstream fs;
    fs.open(path, std::fstream::in);

    int timeCost, from, to;
    auto graph = SPF::GraphReader::readGraph(fs);
    fs >> from >> to >> timeCost;

    fs.close();

    return runAlgorithm(graph, from, to, timeCost);
}

std::vector<std::string> readTxtFiles(std::string path) {
    std::vector<std::string> v;
    path.append("\\*.txt");
    WIN32_FIND_DATA data;
    HANDLE hFind;
    if ((hFind = FindFirstFile(path.c_str(), &data)) != INVALID_HANDLE_VALUE) {
        do {
            v.emplace_back(data.cFileName);
        } while (FindNextFile(hFind, &data) != 0);
        FindClose(hFind);
    }

    return v;
}

void unitTests() {
    std::string inDir = "../res/test/in/";
    std::string outDir = "../res/test/out/";

    std::vector<std::string> inFiles = readTxtFiles(inDir);

    int errors = 0;
    int count = 0;

    for (const auto &file: inFiles) {
        count++;
        auto foundPath = runAlgorithmFromFile(inDir + file);

        int expectedCost, step;
        SPF::Path expectedPath;

        std::fstream expectedFile;
        expectedFile.open(outDir + file, std::fstream::in);

        expectedFile >> expectedCost;
        while (expectedFile >> step) {
            expectedPath.addStep(step);
        }
        expectedPath.setCost(expectedCost);

        if (!expectedFile.is_open()) {
            errors++;
            std::cout << "Couldn't open the file '" << outDir + file << "'" << std::endl;
        } else if (foundPath.getCost() != expectedPath.getCost()) {
            errors++;
            std::cout << "Error in file '" << file << "':" << std::endl;
            std::cout << "Found cost '" << foundPath.getCost() << "' differ form expected cost '"
                                        << expectedPath.getCost() << "'" << std::endl;
            std::cout << "Found path: " << foundPath << std::endl;
            std::cout << "Example path: " << expectedPath << std::endl;
        }

        expectedFile.close();
    }

    if (errors > 0) {
        std::cout << errors << " tests failed" << std::endl;
    } else {
        std::cout << "All " << count << " tests performed successfully" << std::endl;
    }
}

void processMode5(const cxxopts::ParseResult &result) {
    SPF::GraphGenerator gg;
    std::mt19937 e1;
    e1.seed(static_cast<unsigned int>(time(nullptr)));

    unsigned int cities = result["c"].as<unsigned int>();
    unsigned int groups = result["g"].as<unsigned int>();

    unsigned int problems = result["p"].as<unsigned int>();
    unsigned int step = result["s"].as<unsigned int>();
    unsigned int loop = result["l"].as<unsigned int>();

    for (unsigned int p = 0; p < problems; p++) {
        long long averageTime = 0;
        long long edges = 0;
        for (unsigned int l = 0; l < loop; l++) {
            auto graph = gg.generateHardGraph(cities, groups);

//            std::cout << *graph << std::endl;

            int from = 0;
            int to = cities - 1;
            int timeCost = 1;

            TimeVar startTime = timeNow();
            runAlgorithm(graph, from, to, timeCost);
            TimeVar endTime = timeNow();

            averageTime += duration(endTime - startTime);
            edges += graph->getEdgesCount();
            delete graph;
        }
        averageTime = averageTime / loop;
        edges = edges / loop;
        std::cout << "V=" << cities << ", E=" << edges << ": " << averageTime << " ms \n";
        cities += step;
    }
}

